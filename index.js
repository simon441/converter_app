(function() {

    document.querySelector('#output').style.visibility = 'hidden'

    document.querySelector('#dataInput').addEventListener('input', function(e) {
        document.querySelector('#output').style.visibility = 'visible'

        let initial = e.target.value
        document.querySelector('#gramsOutput').innerHTML = (initial / 0.0022046)//.toFixed(2)
        document.querySelector('#kgOutput').innerHTML = (initial / 2.2046)//.toFixed(2)
        document.querySelector('#ozOutput').innerHTML = (initial* 16)//.toFixed(2)
    })
})()
